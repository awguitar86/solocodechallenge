import React, {Component} from 'react';
import './App.css'
import axios from 'axios';
import Select from 'react-select';
import { RepSenOptions, StateOptions } from './Options';

class Main extends Component {
    constructor(props){
        super(props);
        this.state = {
            searchResults: [],
            repSenSelected: '',
            stateSelected: '',
            personSelected: ''
        }
    }

    handleRepSenSelect = selectedOption => {
        this.setState({ repSenSelected: selectedOption });
      };

    handleStateSelect = selectedOption => {
        this.setState({ stateSelected: selectedOption });
    };

    handleSearch = () => {
        const { repSenSelected, stateSelected } = this.state;
        axios.get(`/${repSenSelected.value}/${stateSelected.value}`)
            .then( res => {
                console.log(res.data.results)
                this.setState({ searchResults: res.data.results})
            })
            .catch(err => {throw err});
    }

    handlePersonSelect = (item) => {
        this.setState({ personSelected: item });
        console.log(item);
    }

    render(){
        const { searchResults, repSenSelected, stateSelected, personSelected } = this.state;
        const { name, district, phone, office, link } = this.state.personSelected;
        console.log(this.state);
        const displaySearchResults = searchResults.map( item => {
            const index = searchResults.indexOf(item);
            return (
                <ul className="search-result-item" index={index} key={`Item-${index}`} onClick={() => this.handlePersonSelect(item)}>
                    <li className='name'>{item.name}</li>
                    <li className='party'>{item.party}</li>
                </ul>
            )
        })
        return (
            <div className='main-body'>
                <h2>Who's My Representative?</h2>
                <header>
                    <h4>Search for:&nbsp;&nbsp;</h4>
                    <Select
                        options={RepSenOptions}
                        value={repSenSelected}
                        onChange={this.handleRepSenSelect}
                        className="select rep-sen-select"
                        placeholder='Select...'
                    />
                    <h4>in:&nbsp;&nbsp;</h4>
                    <Select
                        options={StateOptions}
                        value={stateSelected}
                        onChange={this.handleStateSelect}
                        className="select state-select"
                        placeholder='Select...'
                    />
                    <button onClick={this.handleSearch} disabled={!repSenSelected || !stateSelected}>Search</button>
                </header>
                <div className="main-content">
                    <div className="list-of-people">
                        <h3 className='list-h3'>List of <span>{repSenSelected.label}</span></h3>
                        <ul className="search-result-header">
                            <li className='name'><strong>Name</strong></li>
                            <li className='party'><strong>Party</strong></li>
                        </ul>
                        {displaySearchResults}
                    </div>
                    <div className="details">
                        <h3>Info</h3>
                        <p><strong>Name:</strong>&nbsp;{personSelected ? name : null}</p>
                        <p><strong>District:</strong>&nbsp;{personSelected ? district : null}</p>
                        <p><strong>Phone:</strong>&nbsp;{personSelected ? phone : null}</p>
                        <p><strong>Office:</strong>&nbsp;{personSelected ? office : null}</p>
                        <p><strong>Website:</strong>&nbsp;{personSelected ? link : null}</p>
                    </div>
                </div>
            </div>
        )
    }
}

export default Main;